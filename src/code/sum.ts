export default (a, b) => {
  return a + b;
}

export class People {
  name!: string;
  age!: number;
  constructor(_name: string, _age: number) {
    this.name = _name;
    this.age = _age;
  }
  desc() {
    // my name is linguocheng I'm 26 old
    return 'my name is ' + this.name + ' I\'m ' + this.age + ' old';
  }
}