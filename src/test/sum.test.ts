import sum, { People } from '../code/sum'

test('add 1 + 2', () => {
  expect(sum(1, 2)).toBe(3) 
})

test('my name is linguocheng I\'m 26 old', () => {
  const p = new People('linguocheng', 26);
  expect(p.desc()).toBe('my name is linguocheng I\'m 26 old')
})



